import json

from aiohttp import web
import django

django.setup()

from websocket_api.socketio_server import sio


async def index(request):
    """Serve the client-side application."""
    return web.Response(body=json.dumps({'status': 'ok'}), content_type='application/json')


app = web.Application()

sio.attach(app)

app.router.add_get('/', index)

if __name__ == '__main__':
    web.run_app(app)
    print('app started')
