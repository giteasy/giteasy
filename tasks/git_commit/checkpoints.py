from docker.models.containers import ExecResult

from tasks.base import BaseCheckpoint

#
# class AddCheckpoint(BaseCheckpoint):
#     def check(self):
#         result: ExecResult = self.exec('sh -c "git status -s | grep A | wc -l"')
#         return result.output.decode().strip() != '0'
#

class CommitCountCheckpoint(BaseCheckpoint):
    def check(self):
        result: ExecResult = self.exec('sh -c "git log | wc -l"')
        try:
            return int(result.output.decode().strip()) > 1
        except ValueError:
            return False
