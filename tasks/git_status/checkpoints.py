from docker.models.containers import ExecResult

from tasks.base import BaseCheckpoint


class CommitCountCheckpoint(BaseCheckpoint):
    def check(self):
        result: ExecResult = self.exec('sh -c "git log | wc -l"')
        return int(result.output.decode().strip()) > 5
