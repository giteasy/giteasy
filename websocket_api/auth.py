import base64
import json

from django.contrib.sessions.models import Session

from core.models import User


def get_user(socket_environ=None, cookies=None):
    """
    Проверяет авторизацию пользователя по куке из django
    :param socket_environ: environ из сокета
    :return: None - не авторизован, объект пользователя - авторизован
    """


    cookies_header = cookies
    if not cookies and 'HTTP_COOKIE' in socket_environ:
        cookies_header = socket_environ['HTTP_COOKIE']

    session_id = None
    if cookies_header is not None:
        for cookie in cookies_header.split(';'):
            key, val = cookie.strip().split('=')
            if key == 'sessionid':
                session_id = val
                break

    if not session_id:
        return None

    try:
        sess = Session.objects.get(session_key=session_id)
    except Session.DoesNotExist:
        return None

    decoded_data = base64.b64decode(sess.session_data)
    _, session_data = decoded_data.decode().split(':', 1)

    user = json.loads(session_data)

    try:
        user_id = user['_auth_user_id']
    except KeyError:
        return None

    try:
        return User.objects.get(id=user_id)
    except User.DoesNotExist:
        return None
