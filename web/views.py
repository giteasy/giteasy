from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth import logout
from django.db.models import Count
from django.shortcuts import redirect, render_to_response
from vk_api import vk_api

from core.models.task import InputHistory


def main_page_redirect(request):
    return redirect('/')


def logout_user(request):
    logout(request)
    return redirect('/')


def get_vk_info(request):
    social = request.user.social_auth.filter(provider='vk-oauth2').latest('created')
    user = request.user
    user.vk_id = social.extra_data['id']
    user.save()
    return redirect('/')


@staff_member_required
def commands_graph(request):
    list1 = []
    output = []
    commandset = InputHistory.objects.all().values_list('command_type').annotate(Count('command_type'))
    for command in commandset:
        str1 = str(command)
        str1 = str1.replace("'", "")
        str1 = str1.replace("(", "")
        str1 = str1.replace(")", "")
        str1 = str1.split(',')
        list1.append(str1[0])
        list1.append(int(str1[1]))
        output.append(list1)
        list1 = []
    return render_to_response('giteasy/graph.html', {'values': output})
