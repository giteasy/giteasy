from django.urls import path, include

from web.views import commands_graph, main_page_redirect, logout_user, get_vk_info

urlpatterns = [
    path('admin/commands/', commands_graph),
    path('accounts/', include('social_django.urls', namespace='vk')),
    path('accounts/profile/', main_page_redirect),
    path('accounts/logout/', logout_user, name='logout'),
    path('accounts/vk_callback/', get_vk_info, name='profile'),
]
