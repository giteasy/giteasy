import Vue from 'vue'
import Router from 'vue-router'
import Error404 from '../views/Error404'
import Tasks from '../views/Tasks'
import TasksContainer from "../views/TasksContainer";

Vue.use(Router);

export default new Router({
    mode: 'hash',
    routes: [
        {
            path: '/',
            beforeEnter: (to, from, next) => next({name: 'tasks'})
        },
        {
            path: '/profile',
            component: () => import(/* webpackChunkName: "Profile" */ '../views/Profile'),
        },
        {
            path: '/rating',
            component: () => import(/* webpackChunkName: "Rating" */ '../views/Rating')
        },
        {
            path: '/tasks',
            component: TasksContainer,
            children: [
                {
                    path: '',
                    name: 'tasks',
                    component: Tasks
                },
                {
                    path: 'introduction/:id',
                    component: () => import(/* webpackChunkName: "TaskIntroduction" */ '../../tasks/introduction/frontend/Introduction')
                },
                {
                    path: 'git_init',
                    component: () => import(/* webpackChunkName: "TaskGitInit" */ '../../tasks/git_init/frontend/Index.vue')
                },
                {
                    path: 'git_commit',
                    component: () => import(/* webpackChunkName: "TaskCommit" */ '../../tasks/git_commit/frontend/Index.vue')
                },
                {
                    path: 'git_status',
                    component: () => import(/* webpackChunkName: "TaskStatus" */ '../../tasks/git_status/frontend/Index.vue')
                },
                {
                    path: 'git_commit_amend',
                    component: () => import(/* webpackChunkName: "TaskDiff" */ '../../tasks/git_commit_amend/frontend/Index.vue')
                },
                {
                    path: 'git_branches',
                    component: () => import(/* webpackChunkName: "TaskBranches" */ '../../tasks/git_branches/frontend/Index.vue')
                },
                {
                    path: 'git_reset',
                    component: () => import(/* webpackChunkName: "TaskReset" */ '../../tasks/git_reset/frontend/Index.vue')
                },
            ]
        },
        {
            path: '*',
            component: Error404
        }
    ]
})
