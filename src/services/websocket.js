import io from 'socket.io-client';
import store from '../store';
import router from '../router';

let socket;

export function wsConnect() {
    socket = io(`/ws`);

    if (__DEV__) { // в целях отладки даем доступ к сокету из консоли
        window.socket = socket;
        socket.on('connection', data => console.log('connection', data));
        socket.on('connect', data => console.log('connect', data));
        socket.on('disconnect', data => console.log('disconnect', data));
    }

    socket.on('user', data => store.commit('user', data));
    socket.on('need_login', data => {
        store.commit('need_login');
        socket.disconnect();
    });
    socket.on('checkpoints', data => {
        console.log('checkpoints', data);
        store.commit('checkpoints', data)
    });
    socket.on('tasks', data => store.commit('tasks', data));
    socket.on('command_stat', data => store.commit('command_stat', data));
    socket.on('rating', data => store.commit('rating_list', data));
    socket.on('progress', data => store.commit('progress', data));
    socket.on('maxProgress', data => store.commit('max_progress', data));
    socket.on('done', data => {
        console.log('done', data);
        alert('Уровень пройден');
        store.commit('checkpoints', null);
        router.push('/')
    });
}


export function getTasks() {
    socket.emit('tasks');
}

export function getRating() {
    socket.emit('get_rating');
}

export function getCommandStat() {
    socket.emit('get_command_stat');
}

export function startTask(taskName) {
    socket.emit('startTask', taskName);
}

export function startOkOn(callback) {
    socket.on('startOk', data => {
        callback(data);
        socket.off('startOk');
    });
}

export function sendCommand(cmd) {
    socket.emit('cmd', cmd);
}
