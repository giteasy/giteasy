import time

from django.core.management.base import BaseCommand
from core.services.containers import ContainerService


class Command(BaseCommand):
    help = 'Delete user containers'

    def handle(self, *args, **kwargs):
        print('starting cleanup daemon')
        while True:
            ContainerService.delete_user_containers()
            time.sleep(60 * 15)
