from typing import List

from core.models import User, Progress, Task

# Кол-во раз, которое регулирует количество прохождений заданий по метке
# (уникальной команде, ex: "git init"), для закрепления.
# (Используется в рекомендациях и прогрессе пользователя)
MAX_COMMANDS_FOR_REPEAT = 1

# Процент допустимых повторений ранее усвоенных уникальных команд в задании (для пользователя)
# Влияет на рекомендации
COMMANDS_REPEAT_PERCENT = 0.1


def _get_command_rate(completed_progress: List[Progress]):
    commands_dict = {}
    for progress_unit in completed_progress:
        task = progress_unit.task
        for command in task.commands:
            entry = commands_dict.get(command)
            if not entry:
                commands_dict.update({command: 1})
            else:
                commands_dict.update({command: entry + 1})
    return commands_dict


def _get_recommended_task_ids(completed_progress):
    commands_dict = _get_command_rate(completed_progress)
    recommended_task_ids = []
    tasks = Task.objects.all()
    # if len(completed_progress) == 0:
    #     return [tasks.first().id]
    for unit in completed_progress:
        tasks = tasks.exclude(progress=unit)
    for task in tasks:
        fatal_repetitive_flag = False
        repetitive_count = 0
        for command in task.commands:
            entry = commands_dict.get(command)
            if entry:
                if entry > MAX_COMMANDS_FOR_REPEAT:
                    fatal_repetitive_flag = True
                    break
                repetitive_count += 1
        if fatal_repetitive_flag:
            continue
        if len(task.commands) * COMMANDS_REPEAT_PERCENT > repetitive_count:
            recommended_task_ids.append(task.id)
            # break
    return recommended_task_ids


def get_user_tasks_list(user: User):
    user_completed_progress = Progress.objects.filter(user=user, is_done=True)
    recommended_task_ids = _get_recommended_task_ids(user_completed_progress)

    res = []
    for task in Task.objects.all():
        res.append({
            "id": task.id,
            "slug": task.slug,
            "title": task.title,
            "is_done": Progress.objects.filter(user=user, task=task, is_done=True).count() > 0,
            "next_recommended": task.id in recommended_task_ids
        })

    return res
