import datetime
import hashlib

import docker

from core.models import Progress

client = docker.DockerClient(base_url='unix://tmp/docker.sock')


class ContainerService():
    image_tag = 'git_easy_emulator_image'
    dockerfile = './docker/git/Dockerfile'
    workdir = '/sandbox'

    def __init__(self, progress: Progress):
        self.progress = progress
        self.container = None

    def container_name(self):
        keyword = (str(self.progress.id) + str(self.progress.created_time) + str(self.progress.user_id)).encode()
        hashed_keyword = hashlib.md5(keyword).hexdigest()
        return f"giteasy_user_container_{hashed_keyword}"

    def run(self):
        self.container = client.containers.run(
            ContainerService.image_tag,
            name=self.container_name(),
            detach=True,
            tty=True,
            stdin_open=True,
            working_dir=self.workdir,
            remove=True
        )
        print('container create', self.container.logs())

    def get_container(self):
        if not self.container:
            self.container = client.containers.get(self.container_name())
            print('get container', self.container)

    def exec_with_code(self, command):
        self.get_container()
        return self.container.exec_run(command, user='root', workdir=self.workdir)

    def exec(self, command):
        (exit_code, output) = self.exec_with_code(command)
        print(f'command: {command}')
        print(f'output: {output}')
        return output

    def connect(self):
        self.get_container()
        print('get container', self.container)
        return self.container.attach_socket(ws=True)

    @staticmethod
    def delete_user_containers():
        current_date = datetime.datetime.now()
        containers_list = client.containers.list(all=True, filters={'name': 'giteasy_user_container_'})

        for container in containers_list:
            container = client.containers.get(container.short_id)
            container_time = container.attrs.get('Created')
            container_formatted_time = datetime.datetime.strptime(container_time.split('.')[0], "%Y-%m-%dT%H:%M:%S")
            delta_time = (current_date - container_formatted_time).total_seconds() / 60.0
            minutes_after_delete = 30.0
            if delta_time > minutes_after_delete:
                container.remove(force=True)
                print('container', container.attrs.get('Name'), 'deleted')
