from django.db.models import Count

from core.data.commands import COMMANDS_COMPLEXITY
from core.models import InputHistory, User


def get_max_progress() -> int:
    """Получить максимальный прогресс - количество команд, которые должен изучить пользователь"""
    return sum(COMMANDS_COMPLEXITY.values())


def get_user_progress(user: User) -> int:
    """Получить прогресс пользователя - количество команд, которые он использовал во время обучения"""
    sum = 0
    for input_history in InputHistory.objects.filter(user=user).distinct():
        sum += COMMANDS_COMPLEXITY[input_history.command_type]
    return sum


def get_command_stat(user: User):
    """Получить статистику использования команд пользователем"""
    stat = []
    for elem in InputHistory.objects.filter(user=user).values('command_type').annotate(count=Count('command_type')):
        stat.append({
            "command": elem.get("command_type"),
            "count": elem.get("count")
        })

    return stat
